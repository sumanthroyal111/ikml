import requests
import copy
import json
from ikml_utils import Node, ikml_to_anytree, PreOrderIter

class IKML_Document:
    INDENT = 2
    def __init__(self):
        pass

    def load(self, ikml_url):
        self.ikml_url = ikml_url
        self.ikml_data = str(requests.get(self.ikml_url).content, encoding="utf-8")
        self.root = ikml_to_anytree(self.ikml_data.splitlines())

    def save(self, filename="out_ikml.txt"):
        out = "\n".join(Node.tree_as_list(self.root, quoted_attr=False))
        with open(filename, "w", encoding="utf-8") as fd:
            fd.write(out)

    def to_dict(self, recurse_on_children=True):
        # dot-attributes are automatically added for its parent node
        return self.root.to_dict(recurse_on_children=recurse_on_children)

    def to_json(self, recurse_on_children=True):
        d = self.to_dict(recurse_on_children=recurse_on_children)
        return json.dumps(d, ensure_ascii=False, indent=self.INDENT)

    def to_xml(self):
        r2 = copy.deepcopy(self.root)
        r2.put_attrs_inside()
        return r2.to_xml(quoted_attr=True)

    def tags(self):
        out = []
        for n in self.root.node_children:
            # out.append(str(n))
            out.append(n.to_dict(recurse_on_children=False))
        return out

    def child_tags(self, tag_id):
        for node in PreOrderIter(self.root):
            try:
                if node["id"] == tag_id:
                    # return [str(n) for n in node.node_children]
                    return [n.to_dict(recurse_on_children=False) for n in node.node_children]
            except:
                pass
        return f"Node with id {tag_id} not found."

    def get(self, tag_id):
        for node in PreOrderIter(self.root):
            try:
                if node["id"] == tag_id:
                    return node.to_json()
            except:
                pass
        return f"Node with id {tag_id} not found."


def main(ikml_url):
    doc = IKML_Document()
    doc.load(ikml_url)

    # Print top-level tags
    print("\n\nPrint top-level tags - first 10\n\n")
    tags = doc.tags()
    print(tags[:10])

    # To dictionary
    doc_dict = doc.to_dict()

    # Print child tags of a given id
    print(f"\n\nPrint child tags of a given id {tags[0]['id']}\n\n")
    print(doc.child_tags(tags[0]["id"]))

    # Print a given node id
    print(f"\n\nPrint a given node id as JSON {tags[10]['id']}\n\n")
    print(doc.get(tags[10]["id"]))

    # Save
    filename = "out_ikml.txt"
    print(f"Saving as {filename}")
    doc.save(filename)

    # Save as xml
    print(f"Saving as out_xml.txt")
    xmlout = doc.to_xml()
    with open("out_xml.txt", "w", encoding="utf-8") as fd:
        fd.write(xmlout)

if __name__ == "__main__":
    from sys import argv
    try:
        ikml_url = argv[1]
    except:
        ikml_url = "https://siddhantakosha.org/wp-content/smaps/static/granthas/Tarkasangraha-Moola/vakyas-ikml.txt"
    main(ikml_url)    
